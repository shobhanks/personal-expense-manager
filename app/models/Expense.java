package models;

import java.util.Date;
import com.avaje.ebean.*;
import play.data.validation.*;
import play.data.format.*;
import javax.persistence.*;

/**
 * Created by shsharma on 10/3/15.
 */

@Entity
public class Expense extends Model {

    @Id
    public Long id;

    @Constraints.Required
    public String name;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date date = new Date();

    @Constraints.Required
    public double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public static Finder<Long,Expense> find = new Finder<Long,Expense>(
            Long.class, Expense.class
    );


}
