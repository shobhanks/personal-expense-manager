package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import models.Expense;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.db.*;
import views.html.expenses;
import views.html.index;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import play.Logger;
import views.html.thankyou;

/**
 * Created by shsharma on 10/3/15.
 */
public class ExpenseController extends Controller {


    public Result getAllExpenses() {
        DataSource ds = DB.getDataSource();
        Connection connection = DB.getConnection();
        List<Expense> allExpenses = Expense.find.all();

        return ok(expenses.render(allExpenses));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addExpense(){
        JsonNode json = request().body().asJson();

        Logger.debug("In Add Expense " + json.toString());

        Logger.debug("\nLength is " + json.size());

        Logger.debug("\nLast Object's amount is " + json.get(json.size()-1).get("amount"));

        for(int i=0;i<json.size();i++){
            Expense expense = new Expense();
            expense.setName(json.get(i).get("name").asText());
            expense.setAmount(json.get(i).get("amount").asInt());
            expense.setDate(new Date());
            Ebean.save(expense);
        }

        return ok();
    }
}
