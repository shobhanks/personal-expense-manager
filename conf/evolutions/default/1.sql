# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table expense (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  date                      datetime(6),
  amount                    double,
  constraint pk_expense primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table expense;

SET FOREIGN_KEY_CHECKS=1;

